# Bugsnag Error Notification for Coldbox

Log your Coldbox errors using [Bugsnag](https://www.bugsnag.com/) and the [Bugsnag Notify API](https://bugsnagerrorreportingapi.docs.apiary.io/#).

## Configuration

At minimum, you will need a Bugsnag API key. You can get this from the [Bugsnag Dashboard Settings](https://app.bugsnag.com/settings) page.

In `/config/Coldbox.cfc`'s `configure()` method, add `bugsnag.apiKey` setting to the [`moduleSettings` struct](https://coldbox.ortusbooks.com/getting-started/configuration/coldbox.cfc/configuration-directives/modulesettings) like so:

```
function configure(){
    moduleSettings = {
        bugsnag = {
            ApiKey: "PLEASE_SET_ME"
        }
    };
}
```

The following settings are available:

```
moduleSettings = {
    bugsnag = {
        "ApiKey": "REQUIRED",
        // is this app in development, staging, or production?
        "releaseStage": "development",
        // only notify for these environments
        "notifyReleaseStages": ["development","staging","production"], // NOT IMPLEMENTED
        // automatically log items using a Logbox appender
        "enableLogBoxAppender" = true,
        // Min/Max levels for appender
        "levelMin" = "FATAL",
        "levelMax" = "INFO",
        // automatically log exceptions via onException interception event
        "enableExceptionLogging" = true,
        // Data sanitization, scrub fields and headers, replaced with * at runtime
        "scrubFields" 	= [],
        "scrubHeaders" = []
    }
};
```

## TODO

* Follow [Bugsnag integration guidelines](https://docs.bugsnag.com/api/error-reporting/)
* Allow apps to set user and context data per request, to be logged with any errors
* Write tests for BugsnagService and BugsnagAppender

## The Good News

> For all have sinned, and come short of the glory of God ([Romans 3:23](https://www.kingjamesbibleonline.org/Romans-3-23/))

> But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us. ([Romans 5:8](https://www.kingjamesbibleonline.org/Romans-5-8))

> That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved. ([Romans 10:9](https://www.kingjamesbibleonline.org/Romans-10-9/))
 
## Repository

Copyright 2019 (and on) - [Michael Born](https://michaelborn.me/)

* [Homepage](https://bitbucket.org/michaelborn_me/bugsnag/src/master/)
* [Issue Tracker](https://bitbucket.org/michaelborn_me/bugsnag/issues?status=new&status=open)
* [New BSD License](https://bitbucket.org/michaelborn_me/bugsnag/src/master/LICENSE.txt)