/**
*********************************************************************************
* Copyright Since 2005 ColdBox Framework by Luis Majano and Ortus Solutions, Corp
* www.ortussolutions.com
* ---
* Connector to Bugsnag
*/
component accessors=true singleton{
	
	/**
	 * Module  Settings
	 */
	property name="settings" type="struct" inject="coldbox:moduleSettings:bugsnag";
	/**
	 * Module Configuration struct
	 */
	property name="moduleConfig" type="struct" inject="coldbox:moduleConfig:bugsnag";
	/**
	 * API Base URL
	 */	
    property name="APIBaseURL" default="https://notify.bugsnag.com/";

	/**
	 * Constructor
	 * @coldbox.inject coldbox
	 */
	component function init(
		required struct coldbox 
	){
		// coldbox reference
		variables.coldbox 	= arguments.coldbox;

		return this;
	}

	/**
	 * Send a message to Bugsnag
	 * @message A string message
	 * @metadata A struct of metadata to send alongside the message
	 * @level One of: "critical", "error", "warning", "info", defaults to "info"
	 *
	 * @return struct 
	 */
	public struct function sendMessage( required string message, struct metadata={}, string level="info" ){
        var exception = {
            TagContext  : callStackGet(),
            Message     : arguments.message,
            Type        : arguments.level
        };
		return sendToBugsnag(
			exception 	= marshallStackTrace( argumentCollection = exception),
			metadata 	= arguments.metadata,
			level  		= arguments.level
		);
    }
    

	/**
	 * Send an exception to Bugsnag
	 * @exception the coldfusion exception
	 * @metadata A struct of metadata to send alongside the message
	 * @level One of: "critical", "error", "warning", "info", defaults to "info"
	 *
	 * @return struct 
	 */
    public struct function sendException( required any exception, struct metadata={}, string level="error" ) {
		return sendToBugsnag(
			exception 	= marshallStackTrace( argumentCollection = exception),
			metadata 	= arguments.metadata,
			level  		= arguments.level
		);
    }

	/**
	 * Sanitize the incoming http headers in the request data struct
	 * @data The HTTP data struct, passed by reference
	 * @return struct the HTTP headers struct
	 */
	private struct function sanitizeHeaders( required struct data ){
		if( structCount( arguments.data.headers ) ){
			for( var thisHeader in variables.settings.scrubHeaders ){
				// If header found, then sanitize it.
				if( structKeyExists( arguments.data.headers, thisHeader ) ){
					arguments.data.headers[ thisHeader ] = "*";
				}
			}
		}
		return arguments.data;
	}

	/**
	 * Sanitize fields
	 * @data The data fields struct
	 */
	private struct function sanitizeFields( required struct data ){
		if( structCount( arguments.data ) ){
			for( var thisField in variables.settings.scrubFields ){
				// If header found, then sanitize it.
				if( structKeyExists( arguments.data, thisField ) ){
					arguments.data[ thisField ] = "*";
				}
			}
		}
		return arguments.data;
	}

	/**
	 * Sanitize the incoming query string
	 * @target The target string to sanitize
	 * @return string the squeaky clean (scrubbed) querystring
	 */
	private string function sanitizeQueryString( required string target ){
		var aTarget = listToArray( cgi.query_string, "&" )
			.map( function( item, index, array ){
				var key 	= listFirst( arguments.item, "=" );
				var value 	= listLast( arguments.item, "=" );

				// Sanitize?
				if( arrayContainsNoCase( variables.settings.scrubFields, key ) ){
					value = "*";
				}

				return "#key#=#value#";
		} );
		return arrayToList( aTarget, "&" );
	}

	/**
	 * Send a log body to bugsnag
	 * @exception The exception thrown, or exception call stack
	 * @metadata A struct of metadata to send alongside the message
	 * @level One of: "error", "warning", "info", defaults to "info"
	 *
	 * @return struct 
	 */
	private struct function sendToBugsnag( required any exception, struct metadata={}, string level="info" ){
		var threadName 	= "bugsnag-#createUUID()#";
		var event 		= variables.coldbox.getRequestService().getContext();
		var httpData 	= getHTTPRequestData();

		// Sanitize headers
		httpData = sanitizeHeaders( httpData );

		// ColdBox Environment
		var coldboxEnv = {
			"currentEvent"		: event.getCurrentEvent(),
			"currentRoute"		: event.getCurrentRoute(),
			"currentLayout"		: event.getCurrentLayout(),
			"currentView"		: event.getCurrentView(),
			"currentModule"		: event.getCurrentModule(),
			"currentRoutedURL"	: event.getCurrentRoutedURL()
		};

		// Append to custom metadata
		structAppend( arguments.metadata, coldboxEnv, true );

        // Create payload
        // See https://bugsnagerrorreportingapi.docs.apiary.io/#reference/0/notify/send-error-reports
		var payload = {
            "notifier"  : {
                "name"          : "ColdBox Bugsnag Module",
                "version"       : variables.moduleConfig.version,
                "url"           : "https://www.forgebox.io/view/bugsnag"
            },
			"events" 			: [{
                "exceptions": [arguments.exception],
				// Severity level, defaults to "info" for messages
                "severity" 		: lcase(arguments.level),
                // "user": {}
                // application 
                "app": {
                    // "id": ""
                    "version"       : server.coldfusion.productversion,
                    "type"          : server.coldfusion.productname,
                    // app environment
                    "releaseStage"	: variables.settings.releaseStage,
                },
                // client and server info together
                "device"        : {
                    // OS running the app
                    "osName"            : server.os.name,
                    "hostname"          : getHostName(),
                    // "browserName"    : "??"
                    "browserVersion"    : CGI.http_user_agent
                },
				// An identifier for which part of your application this event came from.
				"context" 		: event.getCurrentEvent(),
				// Data about the request this event occurred in.
				"request" 		: {
					// url: full URL where this event occurred, without query string
					"url" 			: listFirst( CGI.REQUEST_URL, "?" ),
					// method: the request method
					"httpMethod" 		: httpData.method,
					// Headers
					"headers" 		: httpData.headers,
					// IP Address of request
                    "clientIp" 		: getRealIP(),
                    // page referred from
                    "referer"       : CGI.HTTP_REFERER,
                    "metaData"      : {
                        // POST: POST params
                        "POST" 			: sanitizeFields( FORM ),
                        // GET: query string params
                        "GET" : sanitizeFields( URL ),
                    }
                },
                // set of steps taken to get to this error / point in the application
                "breadcrumbs": [],
                "threads": [],
				// Custom metadata
				"metaData" : arguments.metadata
			}]
		};

		var apiBaseURL = getAPIBaseURL();

		// thread the http call so we are non-blocking
		thread 
			name="#threadName#" 
			action="run"
			payload=payload
		{
            var sendDateTime = DateTimeFormat( dateConvert( "local2utc", now() ),'yyyy-mm-ddTHH:nn:ssZ');

			var httpCall = new HTTP( url=variables.apiBaseURL, method="POST" );
            httpCall.addParam( type="BODY", value=serializeJSON( payload ) );

            httpCall.addParam( type="header", name="Content-Type", value="application/json" );
            httpCall.addParam( type="header", name="Bugsnag-Payload-Version", value="5" );
            httpCall.addParam( type="header", name="Bugsnag-Api-Key", value=variables.settings.apiKey );
            httpCall.addParam( type="header", name="Bugsnag-Sent-At", value=sendDateTime );
			thread.response = httpCall.send().getPrefix();

			systemOutput( "Sent to bugsnag: #thread.response.toString()#" );
        }
		// return thread information
		return cfthread[ threadName ];
	}

	/**
	 * Marshall a stack trace into an acceptable bugsnag format
	 * This function should be called like `marshallStackTrace( argumentCollection = exception )`
	 * 
	 * @tagContext The call stack from the exception
	 * @message the exception message
	 * @type the type of exception
	 * @detail a more detailed error message
	 * 
	 * @return struct an exception in Bugsnag-friendly format
	 */
	private struct function marshallStackTrace( required array tagContext, required string message, string type = "", string detail = "" ){
		var formatCodeBlock = function( string code = "" ) {
			var lineNumberRE = '[0-9]+:';
			var lineDelimiter = '~~~';
			var lineNumbers = reMatch(lineNumberRE,arguments.code);
			var codeOnly = listToArray(reReplace(arguments.code,lineNumberRE,lineDelimiter,'ALL'),lineDelimiter,true,true);

			var codeBlock = {};
			for ( var n=1; n < ArrayLen(lineNumbers); n++ ) {
				// if exists in array, append to code block as {NUM: "code"}
				codeBlock[lineNumbers[n]] = codeOnly[n];
			}
			return codeBlock;
		};
		/**
		 * Format Frame
		 */
		var formatFrame = function( required struct stackItem ){
			return {
				"file" 	        : arguments.stackItem.template,
				"lineNumber" 	: arguments.stackItem.line ?: arguments.stackItem.lineNumber ?: "",
				"columnNumber" 	: arguments.stackItem.column ?: "",
				"method" 	    : arguments.stackItem.raw_Trace ?: arguments.stackItem.function ?: "",
				// The line of code
				"code" 		    : formatCodeBlock( arguments.stackItem.codePrintPlain ?: "" )
			};
		};

		var trace = {
            "errorClass" 	: arguments.type,
            "message" 		: arguments.message & ' ' & arguments.detail,
			"stacktrace" 	: []
		};

		for( var stackItem in arguments.tagContext ){
			arrayAppend( trace.stacktrace, formatFrame( stackItem ) );
		}

		return trace;
	}

	/**
	 * Get the host name you are on
	 * @return string
	 */
	private string function getHostName(){
		try{
			return createObject( "java", "java.net.InetAddress").getLocalHost().getHostName();
		}
		catch(Any e ){
			return cgi.http_host;
		}
	}

	/**
	* Get Real IP, by looking at clustered, proxy headers and locally.
	* @return string IP address
	*/
	private string function getRealIP(){
		var headers = GetHttpRequestData().headers;

		// Very balanced headers
		if( structKeyExists( headers, 'x-cluster-client-ip' ) ){
			return headers[ 'x-cluster-client-ip' ];
		}
		if( structKeyExists( headers, 'X-Forwarded-For' ) ){
			return headers[ 'X-Forwarded-For' ];
		}

		return len( cgi.remote_addr ) ? cgi.remote_addr : '127.0.0.1';
	}
}