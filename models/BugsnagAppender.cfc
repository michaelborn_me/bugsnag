/**
*********************************************************************************
* Copyright Since 2005 ColdBox Framework by Luis Majano and Ortus Solutions, Corp
* www.ortussolutions.com
* ---
* Appender for Bugsnag leveraging the Bugsnag service
*/
component extends="coldbox.system.logging.AbstractAppender" accessors=true{
	
	/**
	* Constructor
	*/
	component function init( 
		required name,
		struct properties=structnew(),
		layout="",
		numeric levelMin=0,
		numeric levelMax=4
	){
		super.init( argumentCollection=arguments );
		
		// Get Bugsnag Service, wirebox must be in application scope.
		variables.BugsnagService = application.wirebox.getInstance( "BugsnagService@bugsnag" );
		
		return this;
	}

	/**
	 * Log a message
	 */
	public component function logMessage( required coldbox.system.logging.LogEvent logEvent ){
		var metadata = {};
		if ( isJson( arguments.logEvent.getExtraInfo() ) ) {
			metadata = deSerializeJSON(arguments.logEvent.getExtraInfo());
		} else {
			metadata = {
				"info": arguments.logEvent.getExtraInfo()
			}
		}
		var threadStatus = variables.BugsnagService.sendMessage( 
			message = arguments.logEvent.getMessage(),
			metadata = metadata,
			level 	= this.logLevels.lookup( arguments.logEvent.getSeverity() ) 
		);
		return this;
	}

}